
# dark-crystal-secret-sharing-rust

This is part of a work-in-progress Rust implementation of the [Dark Crystal Key Backup Protocol](https://darkcrystal.pw/protocol-specification/).

This crate provides basic authenticated secret-sharing, allowing you to know if recovery was successful.

Internally this uses [sharks](https://docs.rs/sharks/0.5.0/sharks/) for Shamirs secret sharing and [xsalsa20poly1305](https://docs.rs/xsalsa20poly1305/0.8.0/xsalsa20poly1305/) for authenticated encryption.

- [API documentation](https://docs.rs/dark-crystal-secret-sharing-rust)
