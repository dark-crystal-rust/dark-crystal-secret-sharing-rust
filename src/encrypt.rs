//! Basic authenticated encryption using xsalsa20poly1305

use rand::{thread_rng, Rng};
use xsalsa20poly1305::aead::{generic_array::GenericArray, Aead, Error, NewAead};

pub use xsalsa20poly1305::XSalsa20Poly1305;

/// Generate an encryption key
pub fn generate_key() -> xsalsa20poly1305::Key {
    let rng = thread_rng();
    XSalsa20Poly1305::generate_key(rng)
}

/// Encrypt a given plaintext with a given key,
/// and return the nonce used together with the ciphertext
pub fn encrypt(key: xsalsa20poly1305::Key, plaintext: Vec<u8>) -> Result<Vec<u8>, Error> {
    let cipher = XSalsa20Poly1305::new(&key);

    let nonce_bytes = thread_rng().gen::<[u8; 24]>();
    let nonce = GenericArray::from_slice(&nonce_bytes);

    let mut ciphertext_with_nonce = nonce_bytes.to_vec();

    let ciphertext = cipher.encrypt(&nonce, &plaintext[..])?;
    ciphertext_with_nonce.extend(ciphertext);
    Ok(ciphertext_with_nonce)
}

/// Decrypt the given ciphertext with nonce using a given key
pub fn decrypt(
    key: xsalsa20poly1305::Key,
    ciphertext_with_nonce: Vec<u8>,
) -> Result<Vec<u8>, Error> {
    let ciphertext = &ciphertext_with_nonce[24..];
    let nonce = GenericArray::from_slice(&ciphertext_with_nonce[..24]);

    let cipher = XSalsa20Poly1305::new(&key);
    cipher.decrypt(nonce, ciphertext.as_ref())
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn basic() {
        let key = generate_key();
        let message = b"the message";
        let ciphertext = encrypt(key, message.to_vec()).unwrap();
        assert_ne!(&ciphertext, b"the message");
        let plaintext = decrypt(key, ciphertext).unwrap();

        assert_eq!(&plaintext, b"the message");
    }
}
